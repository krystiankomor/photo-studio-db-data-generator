package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/add")
internal class GeneratorController {
    val generators = Generators()

    @Autowired
    lateinit var sprzetRepository: SprzetRepository

    @Autowired
    lateinit var rolaRepository: RolaRepository

    @Autowired
    lateinit var pracownikRepository: PracownikRepository

    @Autowired
    lateinit var klientRepository: KlientRepository

    @Autowired
    lateinit var wypozyczenieRepository: WypozyczenieRepository

    @Autowired
    lateinit var zlecenieRepository: ZlecenieRepository

    @GetMapping("/all")
    @ResponseBody
    public fun generate(@RequestParam rows:Int):String
    {
        val roles: ArrayList<Rola> = arrayListOf()
        val employees: ArrayList<Pracownik> = arrayListOf()
        val clients: ArrayList<Klient> = arrayListOf()
        val devices: ArrayList<Sprzet> = arrayListOf()
        val borrowings: ArrayList<Wypozyczenie> = arrayListOf()
        val jobs: ArrayList<Zlecenie> = arrayListOf()

        for(i in 0 until 6) {
            val rola = generators.generateRola(i)
            roles.add(rolaRepository.save(rola))
        }

        for(i in 1..rows) {
            val pracownik = generators.generatePracownik(roles)
            employees.add(pracownikRepository.save(pracownik))
        }

        for(i in 1..rows) {
            val klient = generators.generateKlient()
            clients.add(klientRepository.save(klient))
        }

        for(i in 1..rows) {
            val device = generators.generateSprzet()
            devices.add(sprzetRepository.save(device))
        }

        for(i in 1..rows) {
            val klient = clients.random()
            val sprzety = mutableSetOf<Sprzet>()
            for(j in 0..10)
            {
                if((0..1).random() == 0)
                {
                    sprzety.add(devices.random())
                }
            }
            val wypozyczenie = generators.generateWypozyczenie(klient, sprzety)
            borrowings.add(wypozyczenieRepository.save(wypozyczenie))
        }

        for(i in 1..rows) {
            val klient = clients.random()
            val pracownik = employees.random()
            val sprzety = mutableSetOf<Sprzet>()
            for(j in 0..10)
            {
                if((0..1).random() == 0)
                {
                    sprzety.add(devices.random())
                }
            }
            val zlecenie = generators.generateZlecenie(klient, pracownik, sprzety)
            jobs.add(zlecenieRepository.save(zlecenie))
        }

        return "Wynegerowano $rows wierszy dla każdej z tabel"
    }

}