package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class GeneratorConfiguration {

    @Bean
    fun databaseInitializer(sprzetRepository: SprzetRepository,
                            rolaRepository: RolaRepository,
                            pracownikRepository: PracownikRepository,
                            klientRepository: KlientRepository,
                            wypozyczenieRepository: WypozyczenieRepository,
                            zlecenieRepository: ZlecenieRepository) = ApplicationRunner {
    }

}