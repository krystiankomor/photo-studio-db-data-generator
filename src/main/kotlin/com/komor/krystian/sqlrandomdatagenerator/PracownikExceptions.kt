package com.komor.krystian.sqlrandomdatagenerator

internal class PracownikNotFoundException(id: Long) : RuntimeException("Could not find pracownik $id")
