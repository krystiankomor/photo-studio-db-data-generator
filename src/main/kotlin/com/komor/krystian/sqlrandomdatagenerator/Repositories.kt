package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.data.repository.CrudRepository


interface SprzetRepository : CrudRepository<Sprzet, Long>

interface PracownikRepository : CrudRepository<Pracownik, Long>

interface RolaRepository : CrudRepository<Rola, Long>

interface KlientRepository : CrudRepository<Klient, Long>

interface WypozyczenieRepository : CrudRepository<Wypozyczenie, Long>

interface ZlecenieRepository : CrudRepository<Zlecenie, Long>