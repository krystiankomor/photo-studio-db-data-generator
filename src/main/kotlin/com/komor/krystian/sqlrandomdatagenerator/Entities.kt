package com.komor.krystian.sqlrandomdatagenerator

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction

import java.sql.Date
import javax.persistence.*

@Entity
data class Klient(
    val nazwa: String,
    val nip: Long,
    val data_utworzenia: Date,
    val email: String,
    val telefon: Int,
    val ulica: String,
    val nr_domu: Int,
    val nr_mieszkania: Int,
    val kod_pocztowy: String,
    val miejscowosc: String,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Long? = null
)


@Entity
data class Sprzet(
    val typ: String,
    val marka: String,
    val model: String,
    val nr_seryjny: String,
    val uwagi: String?,
    val dostepnosc: Boolean,

    @ManyToMany(mappedBy = "sprzet")
    private var wypozyczenia: MutableSet<Wypozyczenie>? = null,

    @ManyToMany(mappedBy = "sprzet")
    private var zlecenia: MutableSet<Zlecenie>? = null,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Long? = null
)

@Entity
data class Rola(
    val nazwa: String,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
)

@Entity
data class Pracownik(
    val imie: String,
    val nazwisko: String,
    val pesel: Long,
    val data_zatrudnienia: Date,
    val telefon: Int,
    val email: String,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "rola_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private var rola: Rola?,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
)

@Entity
data class Wypozyczenie(
    val data_rozpoczenia: Date,
    val data_zakonczenia: Date,
    val uwagi: String?,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="klient_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private val klient: Klient,

    @ManyToMany
    @JoinTable(
            name = "wypozyczenie_sprzet_relation",
            joinColumns = [JoinColumn(name = "wypozyczenie_id", referencedColumnName = "id")],
            inverseJoinColumns = [JoinColumn(name = "sprzet_id", referencedColumnName = "id")])
    private var sprzet: MutableSet<Sprzet>? = null,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
)

@Entity
data class Zlecenie(
    val data_rozpoczenia: Date,
    val data_zakonczenia: Date,
    val uwagi: String?,
    val ulica: String,
    val nr_domu: Int,
    val nr_mieszkania: Int,
    val kod_pocztowy: String,
    val miejscowosc: String,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="pracownik_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private val pracownik: Pracownik,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="klient_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private val klient: Klient,

    @ManyToMany
    @JoinTable(
            name = "zlecenie_sprzet_relation",
            joinColumns = [JoinColumn(name = "zlecenie_id", referencedColumnName = "id")],
            inverseJoinColumns = [JoinColumn(name = "sprzet_id", referencedColumnName = "id")])
    private var sprzet: MutableSet<Sprzet>? = null,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
)
