package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
internal class RolaController(private val repository: RolaRepository) {

    @GetMapping("/rola")
    fun all(): MutableIterable<Rola> {
        return repository.findAll()
    }

    @PostMapping("/rola")
    fun newRola(@RequestBody newRola: Rola): Rola {
        return repository.save(newRola)
    }

}