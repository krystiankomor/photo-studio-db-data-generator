package com.komor.krystian.sqlrandomdatagenerator

import java.sql.Date
import org.springframework.stereotype.Component

@Component
class Generators {
    fun generateRola(iterator: Int):Rola {
        val roles = SampleData.roleNames
        return Rola(
                nazwa = roles[iterator],
                id = (iterator + 1).toLong()
        )
    }

    fun generatePracownik(roles: ArrayList<Rola>):Pracownik {
        val imie = Randomizers.randomFirstName
        val nazwisko = Randomizers.randomLastName
        val emailAddress = Randomizers.generateEmailAddress(imie, nazwisko)
        val phoneNumber = Randomizers.generateRandomPhoneNumber()
        val rola = roles.random()


        return Pracownik(
                imie = imie,
                nazwisko = nazwisko,
                email = emailAddress,
                telefon = phoneNumber,
                data_zatrudnienia = Date(Randomizers.generateRandomTimestamp()),
                pesel = Randomizers.generateRandomPesel(),
                rola = rola)
    }

    fun generateKlient():Klient {
        val nazwa = Randomizers.randomCompanyName
        val emailAddress = Randomizers.generateEmailAddress(nazwa)

        return Klient(
                data_utworzenia = Date(Randomizers.generateRandomTimestamp()),
                email = emailAddress,
                kod_pocztowy = Randomizers.generatePostalCode(),
                miejscowosc = Randomizers.randomCityName,
                nazwa = nazwa,
                nip = Randomizers.generateRandomNip(),
                nr_domu = Randomizers.generateRandomInt(),
                nr_mieszkania = Randomizers.generateRandomFlatNumber(),
                telefon = Randomizers.generateRandomPhoneNumber(),
                ulica = Randomizers.randomStreetName
        )
    }

    fun generateSprzet():Sprzet {
        return Sprzet(
                typ = Randomizers.randomDeviceType,
                marka = Randomizers.randomDeviceBrand,
                model = Randomizers.randomDeviceModel,
                nr_seryjny = Randomizers.generateDeviceSerialNumber((5..8).random()),
                uwagi = null,
                dostepnosc = Randomizers.generateRandomBoolean()
        )
    }

    fun generateWypozyczenie(k:Klient, s:MutableSet<Sprzet>):Wypozyczenie {
        val timestamps:ArrayList<Long> = arrayListOf(Randomizers.generateRandomTimestamp(), Randomizers.generateRandomTimestamp())
        val dataRozpoczecia = Date(timestamps.min()!!)
        val dataZakonczenia = Date(timestamps.max()!!)

        return Wypozyczenie(
                uwagi = null,
                data_rozpoczenia = dataRozpoczecia,
                data_zakonczenia = dataZakonczenia,
                klient = k,
                sprzet = s
        )
    }

    fun generateZlecenie(k:Klient, p:Pracownik, s:MutableSet<Sprzet>):Zlecenie {
        val timestamps:ArrayList<Long> = arrayListOf(Randomizers.generateRandomTimestamp(), Randomizers.generateRandomTimestamp())
        val dataRozpoczecia = Date(timestamps.min()!!)
        val dataZakonczenia = Date(timestamps.max()!!)

        return Zlecenie(
                data_rozpoczenia = dataRozpoczecia,
                data_zakonczenia = dataZakonczenia,
                uwagi = null,
                klient = k,
                pracownik = p,
                sprzet = s,
                nr_domu = Randomizers.generateRandomInt(),
                nr_mieszkania = Randomizers.generateRandomFlatNumber(),
                ulica = Randomizers.randomStreetName,
                miejscowosc = Randomizers.randomCityName,
                kod_pocztowy = Randomizers.generatePostalCode()
        )
    }

}