package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
internal class PracownikController(private val repository: PracownikRepository) {

    // Aggregate root

    @GetMapping("/pracownik")
    fun all(): MutableIterable<Pracownik> {
        return repository.findAll()
    }

    @PostMapping("/pracownik")
    fun newEmployee(@RequestBody newPracownik: Pracownik): Pracownik {
        return repository.save(newPracownik)
    }

    // Single item

    @GetMapping("/pracownik/{id}")
    fun one(@PathVariable id: Long): Pracownik {

        return repository.findById(id)
                .orElseThrow { PracownikNotFoundException(id) }
    }

//    @PutMapping("/pracownik/{id}")
//    fun replaceEmployee(@RequestBody newPracownik: Pracownik, @PathVariable id: Long): Pracownik {
//
//        return repository.findById(id)
//                .map({ pracownik ->
//                    pracownik.setName(newEmployee.getName())
//                    pracownik.setRole(newEmployee.getRole())
//                    repository.save(employee)
//                })
//                .orElseGet({
//                    newEmployee.setId(id)
//                    repository.save(newEmployee)
//                })
//    }

    @DeleteMapping("/pracownik/{id}")
    fun deletePracownik(@PathVariable id: Long) {
        repository.deleteById(id)
    }
}