package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class SqlRandomDataGeneratorApplication

fun main(args: Array<String>) {
	runApplication<SqlRandomDataGeneratorApplication>(*args)
}

