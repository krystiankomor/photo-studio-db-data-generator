package com.komor.krystian.sqlrandomdatagenerator

import javassist.CtMethod.ConstParameter.string
import java.time.Instant
import java.util.*

class Randomizers {
    companion object {
        val randomFirstName get() = SampleData.firstNames.random()
        val randomLastName get() = SampleData.lastNames.random()
        val randomCompanyName get() = SampleData.companyNames.random()
        val randomEmailDomain get() = SampleData.emailDomains.random()
        val randomStreetName get() = SampleData.streetNames.random()
        val randomCityName get() = SampleData.citiesNames.random()
        val randomRoleName get() = SampleData.roleNames.random()
        val randomDeviceModel get() = SampleData.deviceModels.random()
        val randomDeviceType get() = SampleData.deviceTypes.random()
        val randomDeviceBrand get() = SampleData.deviceBrands.random()
        val randomUppercaseLetter get() = SampleData.uppercaseLetters.random()

        fun generateEmailAddress(first_name: String, last_name: String): String {
            return generateEmailAddress("$first_name.$last_name")
        }

        fun generateEmailAddress(name: String): String {
            return "$name@$randomEmailDomain"
        }

        fun generatePostalCode(): String {
            return "${(10..99).random()}-${(100..999).random()}"
        }

        fun generateDeviceSerialNumber(iterations: Int): String {
            var serialNumber = ""
            for (i in 0 until iterations) {
                if ((0..1).random() == 0) {
                    serialNumber += (0..9).random()
                } else {
                    serialNumber += randomUppercaseLetter
                }
            }
            return serialNumber
        }

        fun generateRandomPhoneNumber(): Int {
            return (500000000..999999999).random()
        }

        fun generateRandomTimestamp(): Long {
            return (1262649600..System.currentTimeMillis()).random()
        }

        fun generateRandomPesel():Long {
            return (50000000000..99999999999).random()
        }

        fun generateRandomNip(): Long {
            return (1000000000..9999999999).random()
        }

        fun generateRandomInt():Int {
            return (1..99).random()
        }

        fun generateRandomFlatNumber():Int {
            val isZero = (0..3).random() == 0

            if (isZero) return 0

            return (0..15).random()
        }

        fun generateRandomBoolean():Boolean {
            return (0..1).random() == 0
        }


    }
}