package com.komor.krystian.sqlrandomdatagenerator

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping


@Controller
class WelcomeController {
    @GetMapping("/")
    fun showForm(model: Model):String {
        return "welcome"
    }
}