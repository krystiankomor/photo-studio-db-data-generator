package com.komor.krystian.sqlrandomdatagenerator


class SampleData {
    companion object {
        val firstNames: ArrayList<String>
            get() = arrayListOf("Jan", "Ala", "Łucja", "Krystian", "Dorota", "Andrzej", "Olaf", "Marzena", "Jacek",
                    "Grzegorz", "Rajmund", "Notburga", "Tomasz", "Piotr", "Przemysław")

        val lastNames: ArrayList<String>
            get() = arrayListOf("Kowalski", "Wiśniewski", "Jaki", "Kaczyński", "Petru", "Misiewicz", "Kosek", "Biedroń", "Nowak",
                    "Jonczyk", "Komor", "Łaskawy", "Ładny", "Borusiewicz", "Kulig",
                    "Konieczny", "Kptysek", "Smith", "Obecny", "Nieobecny", "Kulawy")

        val companyNames: ArrayList<String>
            get() = arrayListOf("Kogucik", "STAVA", "INŻYNIERING", "Komor industries", "Magnum", "ABC",
                    "FOKA sp.z.o.o.", "Hurt", "Elektro", "Print", "Kangaroo", "Warrant", "DREW-BUD", "LEN",
                    "POL", "FAST", "MOVIE-BIRD", "MOVIE-BOCIAN")

        val emailDomains: ArrayList<String>
            get() = arrayListOf("gmail.com", "onet.pl", "op.pl", "pudelek.pl", "yahoo.com", "t-mobile.de",
                    "wp.pl", "qwerty.asd", "pole.kk", "polska.pl", "niemcy.pl", "lubie.placki", "nektarynki.as",
                    "yerba.py", "file.cpp", "wedel.pl")

        val streetNames: ArrayList<String>
            get() = arrayListOf("Domańskiego", "Daszyńskiego", "Poliwoda", "Al. Jerozolimskie",
                    "Rondo Okulickiego", "Polskiej Partii", "Powstańców Śląskich", "Powstańców Warszawskich",
                    "Kolejowa", "Sosnowa", "Opolska", "Mazowiecka", "Wrocławska",
                    "Zagrobelna", "Komputerowa", "Prosta", "Zakopana", "Krzywa", "Zielona",
                    "Zwierzęca", "Plac Europejski", "Czekoladowa")

        val citiesNames: ArrayList<String>
            get() = arrayListOf("Kotórz Mały", "Turawa", "Kotórz Wielki", "Zawada",
                    "Warszawa", "Wrocław", "Katowice", "Szczecin",
                    "Toruń", "Zabrze", "Sosnowiec", "Poznań", "Świnoujście",
                    "Zakrzów Turawski", "Ligota Turawska", "Bierdzany", "Pietna", "Wachów", "Olesno",
                    "Wadowice", "Kraków", "Częstochowa", "Ozimek")

        val roleNames: ArrayList<String>
            get() = arrayListOf("Administrator", "Fotograf", "Grafik", "Sekretarka",
                    "Serwisant", "Informatyk")

        val deviceTypes: ArrayList<String>
            get() = arrayListOf("Aparat fotograficzny", "Obiektyw", "Lampa błyskowa", "Statyw",
                    "Blenda", "Kamera")

        val deviceBrands: ArrayList<String>
            get() = arrayListOf("Nikon", "Canon", "Pentax", "Chineese dragon", "Hama")

        val deviceModels: ArrayList<String>
            get() = arrayListOf("A120c", "LIGHT", "BEST MAXI PHOTO", "TURBO", "D800", "D100", "350d", "382a")

        val uppercaseLetters: ArrayList<String>
            get() = arrayListOf("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P")
    }
}